/*
	TestLibrary-wl
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include <QDebug>

#include "qwayland-wayland.h"
#include "qwayland-wlr-layer-shell-unstable-v1.h"

#include "testlib.h"

class TestLibPrivate : public QtWayland::wl_registry {

private:
	Q_DISABLE_COPY(TestLibPrivate)
	Q_DECLARE_PUBLIC(TestLib)

	TestLibPrivate(TestLib *ptr);
	TestLibPrivate(wl_display *wlDisplay, TestLib *ptr);
	~TestLibPrivate();

	void initialize();

	TestLib *q_ptr;
	wl_display *m_wlDisplay;
	zwlr_layer_shell_v1 *m_wlrLayerShell;

protected:
	void registry_global(uint32_t name, const QString &interface, uint32_t version) override;
	void registry_global_remove(uint32_t name) override;

};

TestLibPrivate::TestLibPrivate(TestLib *ptr)
	: q_ptr(ptr)
	, m_wlDisplay(wl_display_connect(nullptr))
{
	qDebug() << "Private Lib constructor";
}

TestLibPrivate::TestLibPrivate(wl_display *wlDisplay, TestLib *ptr)
	: q_ptr(ptr)
	, m_wlDisplay(wlDisplay)
{
	qDebug() << "Private Lib constructor";
}

TestLibPrivate::~TestLibPrivate()
{
	wl_registry_destroy(object());
}

void TestLibPrivate::initialize()
{
	init(wl_display_get_registry(m_wlDisplay));
	wl_display_roundtrip(m_wlDisplay);
}

void TestLibPrivate::registry_global(uint32_t name, const QString &interface, uint32_t version)
{
	Q_UNUSED(version)

	if (interface == zwlr_layer_shell_v1_interface.name) {
		m_wlrLayerShell = static_cast<zwlr_layer_shell_v1 *>(bind(name, &zwlr_layer_shell_v1_interface, 1));

		qDebug() << "Layer shell added";
		if (!m_wlrLayerShell) {
			emit q_ptr->errorOccured(q_ptr->EmptyLayerShell);
		}
	}
}

void TestLibPrivate::registry_global_remove(uint32_t name)
{
	qDebug() << "Registry Global removing..." << name;
}

TestLib::TestLib(QObject *parent)
	: QObject(parent)
	, d_ptr(new TestLibPrivate(this))
{
	qDebug() << "Lib constructor";
}

TestLib::TestLib(wl_display *wlDisplay, QObject *parent)
	: QObject(parent)
	, d_ptr(new TestLibPrivate(wlDisplay, this))
{
	qDebug() << "Lib constructor";
}

TestLib::~TestLib()
{
	delete d_ptr;
}

wl_display *TestLib::wlDisplay()
{
	return d_ptr->m_wlDisplay;
}

void TestLib::initialize()
{
	d_ptr->initialize();
}

QT += widgets

TARGET = testlib
TEMPLATE = lib
DEFINES += TESTLIB_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += . proto

# Link wayland scanner
CONFIG += link_pkgconfig wayland-scanner

# Wayland
QMAKE_USE += wayland-client

WAYLANDCLIENTSOURCES += \
    proto/wayland.xml \
    proto/xdg-shell.xml \
    proto/wayfire-shell-unstable-v2.xml \
    proto/wlr-layer-shell-unstable-v1.xml \
    proto/wlr-foreign-toplevel-management-unstable-v1.xml


SOURCES += \
    testlib.cpp

HEADERS += \
    testlib_global.h \
    testlib.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
    includes.path = /usr/include/testlib/
    includes.files = $$HEADERS#testlib_global.h testlib.h
}
!isEmpty(target.path): INSTALLS += target includes

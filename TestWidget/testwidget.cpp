/*
	TestLibrary-wl
	Copyright (C) 2020 Abrar

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
	USA
*/

#include <QDebug>

#include "testlib.h"


#include "testwidget.h"
#include "ui_testwidget.h"

TestWidget::TestWidget(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::TestWidget)
{
	ui->setupUi(this);

	TestLib *lb = new TestLib();
	connect(lb, &TestLib::errorOccured, [=](TestLib::ErrorType et) {
		qDebug() << "Error" << et;
	});
	lb->initialize();
}

TestWidget::~TestWidget()
{
	delete ui;
}

